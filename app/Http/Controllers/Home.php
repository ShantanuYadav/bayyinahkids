<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Home extends Controller
{
   
   	public function index()
    {
        return view('welcome');
    }

    public function readList()
    {
        $pdfs = \DB::table('media')
            ->select('url','title as file_name','media.id')
            ->join('lessons', function ($join) {
                $join->on('lessons.id', '=', 'media.model_id')
                     ->where('lessons.published',1)
                     ->where('lessons.deleted_at',null);
            })
            ->where('media.model_type','App\Models\Lesson')
            ->where('media.type', 'lesson_pdf')
            ->orderByDesc('lessons.id')
            ->get();
        return view('read/read_list')->with(['pdfs'=>$pdfs]);
    }

    public function readDetail($id)
    {
        $id = base64_decode($id);
        $pdf = \DB::table('media')
                ->select('url','title as file_name','media.id')
                ->leftjoin('lessons', 'lessons.id', '=', 'media.model_id')
                ->where('media.id', $id)
                ->first();
        return view('read/read_detail')->with(['pdf'=>$pdf]);
    }

    public function watchList()
    {
        $videos = \DB::table('media')
            ->select('url','title as file_name','media.id')
            ->join('lessons', function ($join) {
                $join->on('lessons.id', '=', 'media.model_id')
                     ->where('lessons.published',1)
                     ->where('lessons.deleted_at',null);
            })
            ->where('media.model_type','App\Models\Lesson')
            ->where('media.size',0)
            ->orWhere(function($query) {
                $query->where('media.type', 'youtube')
                      ->where('media.type', 'vimeo')
                      ->where('media.type', 'embed')
                      ->where('media.type', 'upload');
            })
            ->orderByDesc('lessons.id')
            ->get();
        
        return view('watch/watch_list')->with(['watch'=>$videos]);
    }

    public function watchDetail($id)
    {
        $id = base64_decode($id);
        $watch = \DB::table('media')
                ->select('url','title as file_name','lessons.id','type')
                ->leftjoin('lessons', 'lessons.id', '=', 'media.model_id')
                ->where('media.id', $id)
                ->first();
                // dd($watch);
        return view('watch/watch_detail')->with(['watch'=>$watch]);
    }

    public function playList()
    {
        return view('play/play_list');
    }

    public function playDetail()
    {
        return view('play/play_detail');
    }

    public function notfound()
    {
        return view('errors/404');
    }

    public function errorfound()
    {
        return view('errors/500');
    }

}
