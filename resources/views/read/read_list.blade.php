@extends('layouts.read_header')

@section('content')
<div id="fullpage">
	<div class="section" id="section1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>illustrated & Audio Books</h1>
					<div class="wrapper read-wrapper">
						<ul>
							@foreach($pdfs as $key)
							<li>
								<a href="{{ url('read-detail/'.base64_encode($key->id))}}">
									<div class="img-wrap">
										<img src="images/read-col.jpg" alt="" class="img-fluid">
										<div class="play-icon">
											<img src="{{ asset('images/play-circle-fill.png')}}" alt="">
										</div>
									</div>
									<div class="read-desc">
										{{$key->file_name}}
									</div>
								</a>
							</li>
							@endforeach
						</ul>
					</div>
					<div class="show-btn text-center">
						<a href="#" class="btn btn-outline-primary">
							Show More
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
