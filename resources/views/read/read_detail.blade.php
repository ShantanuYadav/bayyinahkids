@extends('layouts.read_header')

@section('content')
<div id="fullpage">

	<div class="section read-detail" id="section1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1><a href="{{ url('read-list')}}" class="back-btn"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>{{ $pdf->file_name}}</h1>
						<div class='book_container'>
					  		<div id='book'>
								
								
							</div> 
						</div>
				</div>
			</div>
		</div>
		<div class="show-btn text-center">
						<a href="{{ url('read-list')}}" class="btn btn-outline-primary">
							Show More
						</a>
					</div>
	</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript" src="{{ asset('/wow_book/wow_book.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('/wow_book/pdf.combined.min.js')}}"></script>
<link rel="stylesheet" href="{{ asset('/wow_book/wow_book.css')}}" type="text/css" />
<script type="text/javascript">
  $(document).ready(function() {
    $(function(){
			var bookOptions = {
				 height   : 500
				,width    : 800
				,maxWidth : 990
				,maxHeight : 600

				,centeredWhenClosed : true
				,hardcovers : true
				,numberedPages : [1,-2]
				,pdf: "<?php echo $pdf->url;?>"
				,toolbar : "lastLeft, left, right, lastRight,  zoomin, zoomout, flipsound, fullscreen"
				// ,toolbar : "lastLeft, left, right, lastRight, toc, zoomin, zoomout, slideshow, flipsound, fullscreen, thumbnails, download"

				,thumbnailsPosition : 'bottom'
				,responsiveHandleWidth : 50

				,container: true
				,toolbarPosition: "bottom", // default "bottom"
				style : {
				    color:"white",        // text/icons color
				    background: "#f77758",  // background color
				    separator: "white" ,

				 
				  } // style
				// ,toc: [                      // table of contents in the format
				// 	[ "Item description", 1 ],  // [ "title", page number ]
				// 	[ "Go to codecanyon.net", "http://codecanyon.net" ] // or [ "title", "url" ]
				// ]
			};

			$('#book').wowBook( bookOptions ); // create the book

			var book=$.wowBook("#book"); // get book object instance

			$("#cover").click(function(){
				book.advance();
			});
		});
		});
</script>
@endpush