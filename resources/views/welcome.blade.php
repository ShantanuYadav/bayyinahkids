<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Bayyinah Kids</title>
    
    <!--  GAMING JS AND CSS STARTS  -->

    <meta name="Title" content="Bayyinah Kids" />
        <meta name="description" content="Welcome to the number one Arabic digital literacy platform, where you can enjoy leveled stories, fun cartoon video lessons, interactive exercises, worksheets and fun educational games.">
    <meta name="keywords" content="quiz, game, multiple, knowledge, questions, choice, answers, layout, iq, brain, stupid, idiot">
        
        <!-- for Facebook -->
        <meta property="og:title" content="Bayyinah Kids"/>
        <meta property="og:site_name" content="Bayyinah Kids"/>
        <meta property="og:image" content="http://www.biggorillatest.com/kids/public/images/fav.png" />
        <meta property="og:url" content="http://www.biggorillatest.com/kids/public/play-detail" />
        <meta property="og:description" content="Welcome to the number one Arabic digital literacy platform, where you can enjoy leveled stories, fun cartoon video lessons, interactive exercises, worksheets and fun educational games.">
        
        <!-- for Twitter -->
        <meta name="twitter:card" content="Bayyinah Kids" />
        <meta name="twitter:title" content="Bayyinah Kids" />
        <meta name="twitter:description" content="Welcome to the number one Arabic digital literacy platform, where you can enjoy leveled stories, fun cartoon video lessons, interactive exercises, worksheets and fun educational games." />
        <meta name="twitter:image" content="http://www.biggorillatest.com/kids/public/images/fav.png" />
        
         <link rel="shortcut icon" href="{{ asset('images/logo2.png')}}" type="image/x-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <script>
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement("style");
            msViewportStyle.appendChild(
                document.createTextNode(
                    "@-ms-viewport{width:device-width}"
                )
            );
            document.getElementsByTagName("head")[0].
                appendChild(msViewportStyle);
        }
        </script>

        <link rel="stylesheet" href="{{ asset('css/normalize.css')}}">
        <link rel="stylesheet" href="{{ asset('css/main.css')}}">
        <script src="{{ asset('js/vendor/modernizr-2.6.2.min.js')}}"></script>

        <!--  GAMING CODE ENDS  -->
<!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
</head>

<body>
    <div class="loader-div spinning">
        <img src="{{ asset('images/loader-img.png')}}" class="img-fluid" alt="">
    </div>
    <ul id="menu" class="header-inner nav-bar">
        <li>
            <div class="logo-div">
                <a href="{{ url('/')}}">
                    <img src="{{ asset('images/logo.png')}}" alt="logo" class="img-fluid">
                </a>
            </div>
        </li>
        <li data-menuanchor="secondPage">
            <a href="#section1">
                <div class="tab-wrap">
                    <div class="tab-img">
                        <img src="{{ asset('images/read-img.png')}}" alt="">
                    </div>
                    <div class="tab-text">
                        <p>illustrated & Audio Books</p>
                        <h2>Read</h2>
                    </div>
                </div>
            </a>
        </li>
        <li data-menuanchor="3rdPage">
            <a href="#section2">
                <div class="tab-wrap">
                    <div class="tab-img">
                        <img src="{{ asset('images/watch-img.png')}}" alt="">
                    </div>
                    <div class="tab-text">
                        <p>illustrated & Audio Books</p>
                        <h2>Watch</h2>
                    </div>
                </div>
            </a>
        </li>
        <li data-menuanchor="4thpage">
            <a href="#section3">
                <div class="tab-wrap">
                    <div class="tab-img">
                        <img src="{{ asset('images/play-img.png')}}" alt="">
                    </div>
                    <div class="tab-text">
                        <p>illustrated & Audio Books</p>
                        <h2>Play</h2>
                    </div>
                </div>
            </a>
        </li>
    </ul>
        <header>
            <nav class="navbar navbar-expand-lg">
              <a class="navbar-brand" href="#">
                <img src="{{ asset('images/logo.png')}}" alt="logo">
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Results</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Free</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Trials</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Our Partners</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                  </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                  <button class="btn btn-outline-success my-2 my-sm-0" type="button">Get Started</button>
                </form>
              </div>
            </nav>
    </header>
<div id="fullpage">

    <div class="section banner-section" id="section0">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <h1>Welcome to Bayyinah Kids</h1>
                    <p>Welcome to the number one Arabic digital literacy platform, where you can enjoy leveled stories, fun cartoon video lessons, interactive exercises, worksheets and fun educational games</p>
                    <a href="{{ url('read-list')}}" class="btn btn-primary">
                        Browse
                    </a>
                    
                    @if (Auth::check())
                        <a href="{{ url('user/dashboard')}}" class="btn btn-outline-primary">Dashboard</a>

                    @else
                        <a href="javascript:void" onclick="openModal();" class="btn btn-outline-primary">Log In</a>
                    @endif
                    

                </div>
                <div class="col-md-5">
                    <img src="{{ asset('images/banner-img.png')}}" class="img-fluid banner-seed" alt="">
                </div>
            </div>
            <a data-menuanchor="secondPage" href="#section1" class="scroll-btn">
                <img src="{{ asset('images/scroll.png')}}" alt="">
            </a>
        </div>
    </div>
    <div class="section" id="section1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>illustrated & Audio Books</h1>
                    <div class="wrapper read-wrapper">
                        <ul>
                             @foreach($pdfs as $key)
                            <li>
                                <a href="{{ url('read-detail/'.base64_encode($key->id))}}">
                                    <div class="img-wrap">
                                        <img src="{{ asset('images/read-col.jpg')}}" alt="" class="img-fluid">
                                        <div class="play-icon">
                                            <img src="{{ asset('images/play-circle-fill.png')}}" alt="">
                                        </div>
                                    </div>
                                    <div class="read-desc">
                                        {{$key->file_name}}
                                    </div>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                        
                    </div>
                    <div class="show-btn text-center">
                        <a href="{{ url('read-list')}}" class="btn btn-outline-primary">
                            Show More
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Fun educational videos</h1> 
                    <div class="wrapper watch-wrapper">
                        <ul>
                           @foreach($watch as $key)
                            <li>
                              <a href="{{ url('watch-detail/'.base64_encode($key->id))}}">
                                <div class="img-wrap">
                                  <img src="{{ asset('images/watch-col.png')}}" alt="" class="img-fluid">
                                </div>
                                <div class="watch-desc-wrap">
                                  <div class="watch-desc">
                                    {{$key->file_name}}
                                  </div>
                                  <!-- <div class="watch-cat">
                                    <span>Movies</span>
                                    <span>17.4K views</span>
                                  </div> -->
                                </div>
                              </a>
                            </li>
                            @endforeach
                        </ul>
                        
                    </div>
                    <div class="show-btn text-center">
                        <a href="{{ url('watch-list')}}" class="btn btn-outline-primary">
                            Show More
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="section3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Fun educational games</h1>  
                    <div class="wrapper watch-wrapper play-wrapper">
                        <ul>
                            @for ($i = 0; $i < 8; $i++)
                            <li>
                                <a href="{{ url('play-detail')}}">
                                    <div class="img-wrap">
                                        <img src="{{ asset('images/game1.png')}}" alt="" class="img-fluid">
                                    </div>
                                </a>
                            </li>
                            @endfor
                        </ul>
                        
                    </div>
                    <div class="show-btn text-center">
                        <a href="{{ url('play-list')}}" class="btn btn-outline-primary">
                            Show More
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      <form action="{{ url('login') }}" method="post" id="loginForm" class="container">
        <!-- Modal Header -->
        @csrf
        <div class="modal-header">
          <h4 class="modal-title">Login</h4>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <label for="uname"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" id="email" name="email" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" id="password" name="password" required>
        
        <span style="color: red;" id="error_msg"></span>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <input type="button" class="btn btn-success" onclick="submitModal()" value="Submit">
        </div>
        </form>
      </div>
    </div>
  </div>
  
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{!! csrf_token() !!}' } });
    function openModal(){
        $("#myModal").modal();
    }

    function submitModal(){
        password = $('#password').val();
        email = $('#email').val();

        $.ajax({
          url: "./login",
          type:"POST",
          data:{
            "_token": "{{ csrf_token() }}",
            password:password,
            email:email,
          },
          success:function(response){
            console.log(response);
            if(response.redirect!=''){
                window.location = "./user/dashboard";
            }
            else{
                $('#error_msg').html(response.message);
            }
          },
          error: function(response){
                   const res = response.responseJSON.message;
                    $('#error_msg').html(res);

                }
         });
        };
</script>
<script>
    $(document).ready(function(){
    $(window).bind('scroll', function() {
        var navHeight = $('.banner-section').height();
        if ($(window).scrollTop() > navHeight) {
            $('.nav-bar').addClass('fixed');
         }
        else {
            $('.nav-bar').removeClass('fixed');
         }
    });
});
</script>
 <script>
// Cache selectors
var lastId,
 topMenu = $("#menu"),
 topMenuHeight = topMenu.outerHeight()+1,
 // All list items
 menuItems = topMenu.find("a"),
 // Anchors corresponding to menu items
 scrollItems = menuItems.map(function(){
   var item = $($(this).attr("href"));
    if (item.length) { return item; }
 });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = $(this).attr("href"),
      offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
  $('html, body').stop().animate({ 
      scrollTop: offsetTop
  }, 850);
  e.preventDefault();
});

// Bind to scroll
$(window).scroll(function(){
   // Get container scroll position
   var fromTop = $(this).scrollTop()+topMenuHeight;
   
   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";
   
   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active")
         .end().filter("[href=#"+id+"]").parent().addClass("active");
   }                   
});

  </script>
  <script>

 $(window).load(function() {
    var loaddiv = document.querySelector('.loader-div');
        setTimeout( 
            function  (){  
                loaddiv.classList.remove('spinning');
                
            }, 2000);
});
  </script>
</body>
</html>