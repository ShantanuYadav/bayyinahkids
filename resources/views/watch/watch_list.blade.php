@extends('layouts.watch_header')

@section('content')
<div id="fullpage">

	<div class="section" id="section2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Fun educational videos</h1>	
					<div class="wrapper watch-wrapper">
						<ul>
							@foreach($watch as $key)
							<li>
								<a href="{{ url('watch-detail/'.base64_encode($key->id))}}">
									<div class="img-wrap">
										<img src="{{ asset('images/watch-col.png')}}" alt="" class="img-fluid">
									</div>
									<div class="watch-desc-wrap">
										<div class="watch-desc">
											{{$key->file_name}}
										</div>
										<!-- <div class="watch-cat">
											<span>Movies</span>
											<span>17.4K views</span>
										</div> -->
									</div>
								</a>
							</li>
							@endforeach
						</ul>
						
					</div>
					<div class="show-btn text-center">
						<a href="#" class="btn btn-outline-primary">
							Show More
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection