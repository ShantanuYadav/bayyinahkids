@extends('layouts.watch_header')

@section('content')
<div id="fullpage">

	<div class="section read-detail" id="section2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1><a href="{{ url('watch-list')}}" class="back-btn"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>{{ $watch->file_name}}</h1>	
					<div class="wrapper watch-wrapper video-frame">
						<?php if($watch->type=='upload') {?>
						<video width="853" height="480" controls>
							  <source src="{{$watch->url}}" >
						</video>
						<?php } ?>
						<?php if($watch->type=='youtube') {
							$url= str_replace('watch?v=','embed/', $watch->url);
						
						?>
						<iframe width="853" height="480" src="{{$url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	
						<?php } ?>
						<?php if($watch->type=='vimeo') {
							$url= str_replace('vimeo.com/','player.vimeo.com/video/', $watch->url);
						
						?>
						
						<iframe width="853" height="480" src="{{$url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	
						<?php } ?>

						<?php if($watch->type=='embed') {
							$url= $watch->url;
							echo $url;

							}?>
						
					</div>
					<div class="show-btn text-center">
						<a href="{{ url('watch-list')}}" class="btn btn-outline-primary">
							Show More
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script>
	$( document ).ready(function() {
		var type = "{!! $watch->type !!}";
		var frame = <?php echo $watch->url; ?>;
		if(type!='embed'){
			return false;
		}
		alert(type);
	    console.log( "ready!" );
	  	console.log(frame.src);
});
</script>
@endpush