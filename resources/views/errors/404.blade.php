<!DOCTYPE html>
<html>
<head>
	<title>404</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style type="text/css">
  /*======================
    404 page
=======================*/

.page_404 {
  padding: 40px 0;
  background: #fff;
  font-family: "Arvo", serif;
}

.page_404 img {
  width: 100%;
}

.four_zero_four_bg {
  background-image: url(./images/dribbble_1.gif);
  height: 400px;
  background-position: center;
}

.four_zero_four_bg h1 {
  font-size: 80px;
}

.four_zero_four_bg h3 {
  font-size: 80px;
}

.link_404 {
  color: #fff !important;
  padding: 10px 20px;
  background: #39ac31;
  margin: 20px 0;
  display: inline-block;
}
.contant_box_404 {
  margin-top: -50px;
}

</style>
</head>
<body>

<section class="page_404">
  <div class="container">
    <div class="row">
      <div class="col-md-12 ">
        <div class="col-sm-offset-1 text-center">
          <div class="four_zero_four_bg">
            <h1 class="text-center ">404</h1>
          </div>

          <div class="contant_box_404">
            <h3 class="h2">
              Look like you're lost
            </h3>

            <p>the page you are looking for is not avaible!</p>

            <a href="{{ url('/')}}" class="link_404">Go to Home</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</body>
</html>
