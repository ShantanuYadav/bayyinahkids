<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Bayyinah Kids</title>
		<meta name="Title" content="Bayyinah Kids" />
        <meta name="description" content="Welcome to the number one Arabic digital literacy platform, where you can enjoy leveled stories, fun cartoon video lessons, interactive exercises, worksheets and fun educational games.">
    <meta name="keywords" content="quiz, game, multiple, knowledge, questions, choice, answers, layout, iq, brain, stupid, idiot">
        
        <!-- for Facebook -->
        <meta property="og:title" content="Bayyinah Kids"/>
        <meta property="og:site_name" content="Bayyinah Kids"/>
        <meta property="og:image" content="http://www.biggorillatest.com/kids/public/images/fav.png" />
        <meta property="og:url" content="http://www.biggorillatest.com/kids/public/play-detail" />
        <meta property="og:description" content="Welcome to the number one Arabic digital literacy platform, where you can enjoy leveled stories, fun cartoon video lessons, interactive exercises, worksheets and fun educational games.">
        
        <!-- for Twitter -->
        <meta name="twitter:card" content="Bayyinah Kids" />
        <meta name="twitter:title" content="Bayyinah Kids" />
        <meta name="twitter:description" content="Welcome to the number one Arabic digital literacy platform, where you can enjoy leveled stories, fun cartoon video lessons, interactive exercises, worksheets and fun educational games." />
        <meta name="twitter:image" content="http://www.biggorillatest.com/kids/public/images/fav.png" />
        
         <link rel="shortcut icon" href="{{ asset('images/fav.png')}}" type="image/x-icon">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
</head>

<body class="read-list-inner">
	<div class="loader-div spinning">
		<img src="{{ asset('images/loader-img.png')}}" class="img-fluid" alt="">
	</div>
	<ul id="menu" class="header-inner inner-pages">
		<li>
			<div class="logo-div">
				<a href="{{ url('/')}}">
					<img src="{{ asset('images/logo.png')}}" alt="logo" class="img-fluid">
				</a>
			</div>
		</li>
	    <li data-menuanchor="secondPage" class="active">
	    	<a href="{{ url('read-list')}}">
	    		<div class="tab-wrap">
	    			<div class="tab-img">
	    				<img src="{{ asset('images/read-img.png')}}" alt="">
	    			</div>
	    			<div class="tab-text">
	    				<p>illustrated & Audio Books</p>
	    				<h2>Read</h2>
	    			</div>
	    		</div>
	    	</a>
	    </li>
	    <li data-menuanchor="3rdPage">
	    	<a href="{{ url('watch-list')}}">
	    		<div class="tab-wrap">
	    			<div class="tab-img">
	    				<img src="{{ asset('images/watch-img.png')}}" alt="">
	    			</div>
	    			<div class="tab-text">
	    				<p>illustrated & Audio Books</p>
	    				<h2>Watch</h2>
	    			</div>
	    		</div>
	    	</a>
	    </li>
	    <li data-menuanchor="4thpage">
	    	<a href="{{ url('play-list')}}">
	    		<div class="tab-wrap">
	    			<div class="tab-img">
	    				<img src="{{ asset('images/play-img.png')}}" alt="">
	    			</div>
	    			<div class="tab-text">
	    				<p>illustrated & Audio Books</p>
	    				<h2>Play</h2>
	    			</div>
	    		</div>
	    	</a>
	    </li>
	</ul>
@yield('content')

<script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>

 $(window).load(function() {
 	var loaddiv = document.querySelector('.loader-div');
		setTimeout( 
            function  (){  
                loaddiv.classList.remove('spinning');
                
            }, 2000);
});
  </script>

<script src="{{ asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">
    $('.owl-carousel.owl-theme').owlCarousel({
        loop:true,  
        nav:true,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
  </script>
  @stack('scripts')
</body>
</html>