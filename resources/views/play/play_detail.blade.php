@extends('layouts.play_header')

@section('content')
<div id="fullpage">

	<div class="section" id="section3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Fun educational games</h1>	
					<div class="wrapper watch-wrapper play-wrapper video-frame">
						 <!-- PERCENT LOADER START-->
    	<div id="mainLoader">0</div>
        <!-- PERCENT LOADER END-->
        
        <!-- CONTENT START-->
        <div id="mainHolder">
            <!-- CANVAS START-->
            <div id="canvasHolder">
                <canvas id="gameCanvas" width="1024" height="768"></canvas>
            </div>
            <!-- CANVAS END-->
            
        </div>
        <!-- CONTENT END-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.4.min.js"><\/script>')</script>
        <script src="{{ asset('js/vendor/jquery-1.12.4.min.js')}}"></script>
        <script src="{{ asset('js/vendor/detectmobilebrowser.js')}}"></script>
        <script src="{{ asset('js/vendor/createjs-2015.11.26.min.js')}}"></script>
		<script src="{{ asset('js/vendor/TweenMax.min.js')}}"></script>
        
        <script src="{{ asset('js/plugins.js')}}"></script>
        <script src="{{ asset('js/sound.js')}}"></script>
        <script src="{{ asset('js/canvas.js')}}"></script>
        <script src="{{ asset('js/game.js')}}"></script>
        <script src="{{ asset('js/mobile.js')}}"></script>
        <script src="{{ asset('js/main2.js')}}"></script>
        <script src="{{ asset('js/loader.js')}}"></script>
        <script src="{{ asset('js/init.js')}}"></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-86567323-39"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-424386-45');
        </script>
@endpush
