@extends('layouts.play_header')

@section('content')
<div id="fullpage">

	<div class="section" id="section3">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Fun educational games</h1>	
					<div class="wrapper watch-wrapper play-wrapper">
						<ul>
							@for ($i = 0; $i < 8; $i++)
							<li>
								<a href="{{ url('play-detail')}}">
									<div class="img-wrap">
										<img src="{{ asset('images/game1.png')}}" alt="" class="img-fluid">
									</div>
								</a>
							</li>
							@endfor
						</ul>
						
					</div>
					<div class="show-btn text-center">
						<a href="#" class="btn btn-outline-primary">
							Show More
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection