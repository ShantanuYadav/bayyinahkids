////////////////////////////////////////////////////////////
// CANVAS LOADER
////////////////////////////////////////////////////////////

 /*!
 * 
 * START CANVAS PRELOADER - This is the function that runs to preload canvas asserts
 * 
 */
function initPreload(){
	toggleLoader(true);
	
	checkMobileEvent();
	
	$(window).resize(function(){
		resizeGameFunc();
	});
	resizeGameFunc();
	
	loader = new createjs.LoadQueue(false);
	manifest=[{src:'public/assets/logo.png', id:'logo'},
			{src:'public/assets/brain_score.png', id:'brainScore'},
			{src:'public/assets/brain_result.png', id:'brainResult'},
			{src:'public/assets/icon_facebook.png', id:'iconFacebook'},
			{src:'public/assets/icon_twitter.png', id:'iconTwitter'},
			{src:'public/assets/icon_whatsapp.png', id:'iconWhatsapp'},
			{src:'public/assets/arrow.png', id:'arrow'},
			{src:'public/assets/loader_Spritesheet5x5.png', id:'loader'},
			{src:'public/assets/brainIdea_Spritesheet4x4.png', id:'brainCorrect'},
			{src:'public/assets/brainWrong_Spritesheet4x4.png', id:'brainWrong'},
			
			{src:'public/assets/button_confirm.png', id:'buttonConfirm'},
			{src:'public/assets/button_cancel.png', id:'buttonCancel'},
			{src:'public/assets/item_exit.png', id:'itemExit'},
			{src:'public/assets/button_fullscreen.png', id:'buttonFullscreen'},
			{src:'public/assets/button_sound_on.png', id:'buttonSoundOn'},
			{src:'public/assets/button_sound_off.png', id:'buttonSoundOff'},
			{src:'public/assets/button_exit.png', id:'buttonExit'},
			{src:'public/assets/button_settings.png', id:'buttonSettings'}];
	
	soundOn = true;		
	if($.browser.mobile || isTablet){
		if(!enableMobileSound){
			soundOn=false;
		}
	}
	
	if(soundOn){
		manifest.push({src:'public/assets/sounds/whoosh.ogg', id:'soundWhoosh'})
		manifest.push({src:'public/assets/sounds/select.ogg', id:'soundSelect'})
		manifest.push({src:'public/assets/sounds/selectAnswer.ogg', id:'soundSelectAnswer'})
		manifest.push({src:'public/assets/sounds/scoreBrainIdea.ogg', id:'soundScoreBrainIdea'})
		manifest.push({src:'public/assets/sounds/wrong.ogg', id:'soundWrong'})
		manifest.push({src:'public/assets/sounds/scoreBrain.ogg', id:'soundScoreBrain'})
		manifest.push({src:'public/assets/sounds/fail.ogg', id:'soundFail'})
		manifest.push({src:'public/assets/sounds/complete.ogg', id:'soundComplete'})
		manifest.push({src:'public/assets/sounds/musicMain.ogg', id:'musicMain'})
		manifest.push({src:'public/assets/sounds/musicGame.ogg', id:'musicGame'})
		
		createjs.Sound.alternateExtensions = ["mp3"];
		loader.installPlugin(createjs.Sound);
	}
	
	loader.addEventListener("complete", handleComplete);
	loader.addEventListener("fileload", fileComplete);
	loader.addEventListener("error",handleFileError);
	loader.on("progress", handleProgress, this);
	loader.loadManifest(manifest);
}

/*!
 * 
 * CANVAS FILE COMPLETE EVENT - This is the function that runs to update when file loaded complete
 * 
 */
function fileComplete(evt) {
	var item = evt.item;
	//console.log("Event Callback file loaded ", evt.item.id);
}

/*!
 * 
 * CANVAS FILE HANDLE EVENT - This is the function that runs to handle file error
 * 
 */
function handleFileError(evt) {
	console.log("error ", evt);
}

/*!
 * 
 * CANVAS PRELOADER UPDATE - This is the function that runs to update preloder progress
 * 
 */
function handleProgress() {
	$('#mainLoader').html(Math.round(loader.progress/1*100)+'%');
}

/*!
 * 
 * CANVAS PRELOADER COMPLETE - This is the function that runs when preloader is complete
 * 
 */
function handleComplete() {
	toggleLoader(false);
	initMain();
};

/*!
 * 
 * TOGGLE LOADER - This is the function that runs to display/hide loader
 * 
 */
function toggleLoader(con){
	if(con){
		$('#mainLoader').show();
	}else{
		$('#mainLoader').hide();
	}
}